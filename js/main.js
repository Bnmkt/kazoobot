module.exports = {
    vars: {
        config: require('./config.json'),
        titleDownloaded: [],
        titleInPlaylist: [],
        playlistId: "PLJDL1Yw9UTdoQCFpzwTlxKHp6ZYk10PK8"
    },
    Discord: require('discord.js'),
    client: undefined,
    playMusic(connection, music) {
        const dispatcher = connection.play(`music/${music}`)
        dispatcher.pause();
        dispatcher.resume();
        console.log(`Playing ${music}`)
        dispatcher.on('finish', () => {
            const titleToPlay = this.vars.titleDownloaded[Math.floor(Math.random() * this.vars.titleDownloaded.length)]
            this.playMusic(connection, titleToPlay)
        })
    },
    init() {
        const client = this.client = new this.Discord.Client()
        client.on('ready', () => {
            const { getInfo } = require("ytdl-getinfo")
            const ytdl = require("ytdl-core")
            const fs = require('fs')
            getInfo(this.vars.playlistId, [], true).then(info => {
                console.log(info)
                info.items.forEach((inf, i) => {
                    this.vars.titleInPlaylist.push({ "id": inf.id, "title": inf.title })
                    console.log("loading " + inf.title)
                    this.vars.titleDownloaded = fs.readdirSync('./music').filter(file => file.endsWith('.mp3'));
                })
                this.vars.titleInPlaylist.forEach(title => {
                    if (this.vars.titleDownloaded.indexOf(title.id + ".mp3") === -1) {
                        console.log("downloading " + title.title)
                        ytdl(`https://www.youtube.com/watch?v=${title.id}`, { filter: 'audioonly' }).pipe(fs.createWriteStream(`./music/${title.id}.mp3`));
                    } else {
                        console.log("Already downloaded " + title.title)
                    }
                })
                console.log(`Logged in as ${client.user.tag}!`);
            })
        });

        client.on('message', async message => {
            if (message.mentions && message.mentions.users.array()[0].id == client.user.id) {
                if (message.member.voice.channel.members.find(user => user.id == client.user.id)) {
                    message.member.voice.channel.leave()
                    console.log("cheh")
                } else {
                    const connection = await message.member.voice.channel.join();
                    const titleToPlay = this.vars.titleDownloaded[Math.floor(Math.random() * this.vars.titleDownloaded.length)]
                    this.playMusic(connection, titleToPlay)
                }
            }
        });

        client.login(this.vars.config.token);
    }
}